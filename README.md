# Panospheres

blender 4.3+ add-on to import and manipulate panoramas for landscape simulations.

Supports both equirectangular and [central cylindrical](https://en.wikipedia.org/wiki/Central_cylindrical_projection) projections for source images and camera types.

# How to use:

Once installed from the zip file, the UI is located in the 3D View's side panel (<kbd>N</kbd>), in the <kbd>Panospheres</kbd> tab, which you can rename in the add-on's preferences if, for example, you want to put it in an already existing tab to save tab space.

## Importing panoramas:
### Single import mode:
Tooltips on mouse hover should help you. For more details:
- **Ground parent**: parents the camera to an empty and moves the camera above it by the set value. This allows you to snap the empty to your terrain and the camera will stay at the same height above the ground. The panorama, while not parented, is constrained to the position of the camera. This allows to rotate the camera without rotating the panorama.


### Batch import mode:
**The following workflow uses [QGIS](https://www.qgis.org/en/site/), [blenderGIS](https://github.com/domlysz/BlenderGIS) and a spreadsheet program like [LibreOffice Calc](https://www.libreoffice.org/download/download-libreoffice/) to create the input data:**
1. In a spreadsheet, create a table like this (the column names must be exact, except for X and Y):

|X|Y|name|image_path|HFOV|rotation|altitude|height|sun_elev|sun_rot|
|-|-|-|-|-|-|-|-|-|-|
|1.28014|47.23500|01|my/folder/image01.jpg|180|214.6|211.4|1.7|18|51|
|1.52195|47.15563|02|my/folder/image02.jpg|163|54.9|164|1.45|21|315|
2. paste the table in a text file. The column separator character can be a tab or anything else.
3. **in QGIS** :
    1. import the table using "Add a delimited text layer". It is recommended - though not problematic if you don't - to disable "Detect Field types" in order to avoid panorama names like "02" becoming "2.0" once imported in Blender.
    2. Make sure your points are properly positionned on the map
    3. export the layer as a shapefile
3. **in Blender**:
    1. using BlenderGIS, import the shapefile while enabling:
        - separate objects
        - object name from field : then select "name" in the dropdown
    2. in the Panospheres tab :
        1. set Mode to Batch
        2. enable whichever checkboxes you want
        3. for the Viewpoints Collection: select the imported shapefile's collection
        4. click on Import Panoramas

## Manipulating panoramas:

Once a panorama is imported you can manipulate it to align photographed features to their 3D version:

1. enable Material viewport shading
2. collapse the Import panel to clear the interface
3. select the panosphere object : controls should appear in the Manipulate panel. These controls are actually input values for its Geometry Nodes modifier, which contains more input parameters. For the sliders that are not self-explanatory: 
    - **HFOV** : horizontal field of view of the image
    - **Conical Pitch** (this term is one I came up with): think of it as either a correction for a panorama where each individual photo is tilted forward or backwards (pitch) while Hugin didn't catch it, or a compensation for a vertically cropped panorama. The imaginary disc representing the panorama's horizon around the viewpoint then becomes a cone. That's the reason for the name. [These gifs](https://groups.google.com/g/hugin-ptx/c/azklf2rdW40/m/IxisD8pGAQAJ) should make things clearer. Otherwise just slide it and see what happens.
    - **Resolution** : the mesh density of the sphere. This density adapts to the HFOV and VFOV to remain the same. Use Wireframe mode to see it.
4. to reorient the panorama:
    1. click on "Set up Manipulation", this will enable settings so you can easily manipulate the panorama
    2. jump in the generated camera
    3. you can scale up the panorama if you want the terrain to be in front
    4. left click somewhere on the image in the 3D View : this will place the cursor on it
    5. use Blender's rotation tool this way:
        - Roll : <kbd>R</kbd>+<kbd>Z</kbd> rotates the panorama around the cursor
        - Pitch : <kbd>R</kbd>+<kbd>X</kbd> or <kbd>R</kbd>+<kbd>Y</kbd> (depends on where your cursor is. The behavior is not very predictable) rotates the panorama up or down from the cursor
        - Yaw : <kbd>R</kbd>+<kbd>Z</kbd>+<kbd>Z</kbd> or simply sliding the Z rotation in the object's properties
5. **in Hugin**: once your panorama is oriented the way you want, you can copy the corrective results for Hugin (in purple) using their respective copy-to-clipboard buttons and paste them in Hugin's Yaw Pitch Roll fields in the "Move/Drag" tab of the GL window. ⚠ Only hit Apply once you've pasted all the values (although pasting the Yaw is optional), because the rotation order matters. You can then re-stitch your panorama, and reset its rotation to 0 in Blender (except for the Z rotation if you didn't report the Yaw to Hugin). The photo's elements should be at the same place relative to the 3D scene after that.

That's the main gist of it.
